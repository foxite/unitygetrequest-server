<?php
// This API script requires the parameters "username", "password", "email".
include("../includes/apiresponse.php");

if (empty($_POST["username"]) ||
	empty($_POST["password"]) ||
	empty($_POST["email"])) {
	die(respond_failure("client_error", "Error communicating with the server. (A3-0)"));
}

require_once("../includes/dbconnect.php");

$username = $conn->real_escape_string($_POST["username"]);
$email = $conn->real_escape_string($_POST["email"]);

if ($conn->query("SELECT username FROM accounts WHERE username = '$username' LIMIT 1")->num_rows > 0) {
	echo(respond_failure("username_exists", "That username is already taken."));
} else if ($conn->query("SELECT username FROM accounts WHERE email = '$email' LIMIT 1")->num_rows > 0) {
	echo(respond_failure("email_exists", "That email is already taken."));
} else {
	$hashedPw = password_hash($_POST["password"], PASSWORD_BCRYPT);

	$sql = "INSERT INTO accounts (username, email, password)
	VALUES ('$username', '$email', '$hashedPw')";

	if ($conn->query($sql) === true) {
		echo(respond_success("created"));
	} else {
		echo(respond_failure("server_error", "The server encountered an error. (A3-1)"));
	}
}

?>