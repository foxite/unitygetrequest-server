<?php

include("../includes/apiresponse.php");

if (empty($_POST["token"])) {
	die(respond_failure("client_error", "Error communicating with the server. (A2-0)"));
}

require_once("../includes/dbconnect.php");

// Check token
$token = $conn->real_escape_string($_POST["token"]);
$sql = "SELECT id, account FROM account_tokens WHERE token = '$token' LIMIT 1";
$tokenResult = $conn->query($sql);
if ($tokenResult->num_rows === 1) {
	// Token found: check against user
	$tokenRow = $tokenResult->fetch_assoc();
	$sql = "SELECT id, active_token FROM accounts WHERE id = {$tokenRow["account"]}";
	$accountResult = $conn->query($sql);
	if ($accountResult->num_rows === 1) {
		// Found user
		$accountRow = $accountResult->fetch_assoc();

		// Change the active_token to NULL
		$sql = "UPDATE accounts SET active_token = NULL WHERE id = {$accountRow["id"]}";
		$conn->query($sql);

		// Delete token
		$sql = "DELETE FROM account_tokens WHERE id = {$tokenRow["id"]}";
		$conn->query($sql);

		echo(respond_success("deleted"));
	} else {
		echo(respond_failure("token_invalid", "Your login token is invalid. (A2-1)"));
	}
} else {
	echo(respond_failure("token_invalid", "Your login token is invalid. (A2-2)"));
}
