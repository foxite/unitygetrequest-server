<?php
// This API script requires the parameters "username", "password", "email".

define("TOKEN_LENGTH", 128);
function create_new_token($conn, $id) {
	$characters = "QWERTYUIOPASDFGHJKLZXCVBNMqwertyuiopasdfghjklzxcvbnm1234567890";
	$token = "";
	for ($i = 0; $i < TOKEN_LENGTH; $i++) {
		$token .= $characters[rand(0, strlen($characters) - 1)];
	}

	$sql = "INSERT INTO account_tokens (account, token, last_used)
	VALUES ($id, '$token', now())";
	$result = $conn->query($sql);
	$newTokenId = $conn->insert_id;
	
	$sql = "UPDATE accounts SET active_token = $newTokenId WHERE id = $id";
	$conn->query($sql);

	return $token;
}

include("../includes/apiresponse.php");

if (empty($_POST["username"]) ||
	empty($_POST["password"])) {
		die(respond_failure("client_error", "Error communicating with the server. (A1-0)"));
}

require_once("../includes/dbconnect.php");

$username = $conn->real_escape_string($_POST["username"]);

// Find account
$sql = "SELECT id, password, active_token FROM accounts WHERE username = '$username' LIMIT 1";
$result = $conn->query($sql);
if ($result->num_rows === 1) {
	$user = $result->fetch_assoc();
	// Check password
	if (password_verify($_POST["password"], $user["password"])) {
		// Check token
		if ($user["active_token"] === null) {
			// User is not active: create new token and return to client
			echo(respond_success(create_new_token($conn, $user["id"])));
		} else {
			// User has active token: check if active token is older than 5 minutes (timed out)
			$sql = "SELECT last_used FROM account_tokens WHERE id = {$user["active_token"]} LIMIT 1";
			$tokenResult = $conn->query($sql);
			if ($tokenResult->num_rows === 1) {
				// Token row found
				$existingToken = $tokenResult->fetch_assoc();
				$tokenLastUsed = date_create($existingToken["last_used"]);
				if (time() - $tokenLastUsed->getTimestamp() > 300) { // 300 seconds == 5 minutes
					// Token inactive for more than 5 minutes: create new
					echo(respond_success(create_new_token($conn, $user["id"])));
				} else {
					// Token active within the last 5 minutes: do not create new
					echo(respond_failure("session_active", "This account already has a session active. Try again in 5 minutes or contact customer support. (A1-1)"));
				}
			} else {
				// Should not happen, but if it does, trying to log in again should fix it.
				echo(respond_failure("server_error", "The server encountered an error. Please try again, or contact customer support. (A1-2)"));
			}
		}
	} else {
		echo(respond_failure("credentials", "Username or password are invalid."));
	}
} else {
	echo(respond_failure("credentials", "Username or password are invalid."));
}

?>