<?php
// This include file writes to $conn. It requires config.php once. It can die.

require_once("config.php");
require_once("apiresponse.php");

$conn = new mysqli(DB_SERV, DB_USER, DB_PASS, DB_NAME);
if ($conn->connect_error) {
	die(respond_failure("The server is not available right now. (I3-0)"));
}

?>