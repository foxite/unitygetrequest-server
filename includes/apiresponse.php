<?php
// This include file defines the functions: respond_failure(2) and respond_success(1).

function respond_failure($reason, $message) {
	return json_encode(array(
		"success" => false,
		"reason" => $reason,
		"message" => $message));
}

function respond_success($data) {
	return json_encode(array(
		"success" => true,
		"data" => $data));
}

?>