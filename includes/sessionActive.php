<?php
// This include file checks if $_POST["token"] matches a currently usable token, and dies otherwise, with an error message for the client.
// It includes apiresponse.php once and requires dbconnect.php once.
// It writes to $userId and $sessionActiveData.

$sessionActiveData = "";
$userId = -1;

include_once("apiresponse.php");

if (!isset($_POST["token"]) || empty($_POST["token"])) {
	die(respond_failure("client_error", "Required parameter missing (I4-0)"));
}

require_once("dbconnect.php");

// Check token
$token = $conn->real_escape_string($_POST["token"]);
$sql = "SELECT id, account FROM account_tokens WHERE token = '$token' LIMIT 1";
$tokenResult = $conn->query($sql);
if ($tokenResult->num_rows === 1) {
	// Token was found: check against user
	$tokenRow = $tokenResult->fetch_assoc();
	$sql = "SELECT id, active_token FROM accounts WHERE id = {$tokenRow["account"]} LIMIT 1";
	$accountResult = $conn->query($sql);

	if ($accountResult->num_rows === 1) {
		// Found the user: check if this is the active token
		$accountRow = $accountResult->fetch_assoc();
		$userId = $accountRow["id"];

		if ($accountRow["active_token"] === null) {
			$sessionActiveData = "last_session_inactive";
			$sql = "UPDATE account_tokens SET last_used = now() WHERE id = {$tokenRow["id"]}";
			$conn->query($sql);

			$sql = "UPDATE accounts SET active_token = '{$token}' WHERE id = {$accountRow["id"]}";
			$conn->query($sql);
		} else {
			$sql = "SELECT id, token, last_used FROM account_tokens WHERE id = {$accountRow["active_token"]} LIMIT 1";
			$activeTokenResult = $conn->query($sql);
			if ($activeTokenResult->num_rows === 1) {
				$activeTokenRow = $activeTokenResult->fetch_assoc();
				
				if ($activeTokenRow["token"] == $token) {
					// This is the active token
					$sessionActive = true;
					$sessionActiveData = "active";
					$sql = "UPDATE account_tokens SET last_used = now() WHERE id = {$activeTokenRow["id"]}";
					$conn->query($sql);
				} else {
					// This is not the active token: check if the active token has been inactive for more than 5 minutes
					$tokenLastUsed = date_create($activeTokenRow["last_used"]);
					if ((time() - $tokenLastUsed->getTimestamp()) > 300) { // 300 seconds == 5 minutes
						$sessionActive = true;
						$sessionActiveData = "last_session_inactive";
						$sql = "UPDATE account_tokens SET last_used = now() WHERE id = {$activeTokenRow["id"]}";
						$conn->query($sql);
					} else {
						die(respond_failure("session_active", "A session is active on your account. Try again in 5 minutes or contact customer support. (I4-0)"));
					}
				}
			} else {
				// Should not happen, but if it does, trying to log in again should fix it.
				die(respond_failure("server_error", "The server encountered an error. Please try to log in again, or contact customer support. (I4-1)"));
			}
		}
	} else {
		die(respond_failure("token_invalid", "Your login token is invalid."));
	}
} else {
	die(respond_failure("token_invalid", "Your login token is invalid."));
}
