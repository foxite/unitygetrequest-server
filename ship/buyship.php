<?php

require_once("../includes/sessionActive.php");
require_once("../includes/apiresponse.php");
require_once("../includes/dbconnect.php");

if (empty($_POST["type"]) || !is_numeric($_POST["type"]) || intval($_POST["type"]) < 0) {
	die(respond_failure("client_error", "Required parameter missing (S1-0)"));
}

$type = intval($_POST["type"]);

$sql = "SELECT cost FROM ship_types WHERE id = {$type} LIMIT 1";
$typeResult = $conn->query($sql);
if ($typeResult->num_rows === 1) {
	$typeRow = $typeResult->fetch_assoc();

	$sql = "SELECT money FROM accounts WHERE id = {$userId} LIMIT 1";
	$accountResult = $conn->query($sql);
	if ($accountResult->num_rows === 1) {
		$accountRow = $accountResult->fetch_assoc();

		$newMoney = intval($accountRow["money"]) - intval($typeRow["cost"]);
		if ($newMoney >= 0) {
			// Buy
			$sql = "UPDATE accounts SET money = {$newMoney} WHERE id = {$userId}";
			$conn->query($sql);
			
			$sql = "INSERT INTO ships (owner, ship_type) VALUES ($userId, $type)";
			$conn->query($sql);
			$newShipId = $conn->insert_id;
			echo(respond_success($newShipId));
		} else {
			// Not enough money
			echo(respond_failure("insufficient_money", "You don't have enough money to buy this ship."));
		}
	} // Since we have required sessionActive we can assume that $userId is a valid user.
} else {
	echo(respond_failure("invalid_type", "That ship type does not exist. (S1-1)"));
}