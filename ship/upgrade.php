<?php

require_once("../includes/sessionActive.php");
require_once("../includes/apiresponse.php");
require_once("../includes/dbconnect.php");

$upgrades = array(
	"hull" => -1,
	"armor" => -1,
	"speed" => -1,
	"kinetic" => -1,
	"laser" => -1,
	"missile" => -1,
	"missile_count" => -1
);

function check($name) {
	global $upgrades;
	$ret = empty($_POST[$name]) || !is_numeric($_POST[$name]) || intval($_POST[$name]) < 0;
	if (!$ret) {
		$upgrades[$name] = intval($_POST[$name]);
	}
	return $ret;
}

if (check("id", $upgrades) | (
	check("hull") & check("armor") & check("speed") & check("kinetic") & 
	check("laser") & check("missile") & check("missile_count"))) {
	die(respond_failure("client_error", "Required parameter missing (S1-0)"));
}

$sql = "SELECT money FROM accounts WHERE id = {$userId} LIMIT 1";
$accountResult = $conn->query($sql);
if ($accountResult->num_rows === 1) {
	$accountRow = $accountResult->fetch_assoc();

	$shipId = intval($_POST["id"]);
	
	$sql = "SELECT * FROM ships WHERE id = {$shipId} LIMIT 1";
	$shipResult = $conn->query($sql);
	if ($shipResult->num_rows === 1) {
		$shipRow = $shipResult->fetch_assoc();
		$cost = 0;

		$upgradeSql = "";
		$first = true;
		foreach ($upgrades as $upgrade => $increase) {
			if ($increase !== -1 && $upgrade != "id") {
				$existing = intval($shipRow[$upgrade . "_upgrades"]);
				for ($i = $existing; $i < $existing + $increase; $i++) { 
					$cost += 10 * $existing;
				}
				
				$existing = intval($shipRow[$upgrade . "_upgrades"]);
				$newTotal = $existing + $increase;
				if (!$first) {
					$upgradeSql .= ",";
				}
				$first = false;
				$upgradeSql .= " " . $upgrade . "_upgrades = " . $increase . "";
			}
		}

		if (intval($accountRow["money"]) >= $cost) {
			$newMoney = intval($accountRow["money"]) - $cost;
			// Buy
			$sql = "UPDATE accounts SET money = {$newMoney} WHERE id = {$userId}";
			$conn->query($sql);
			
			$sql = "UPDATE ships SET" . $upgradeSql . " WHERE id = {$shipId}";

			$conn->query($sql);
			echo(respond_success($newMoney));
		} else {
			// Not enough money
			echo(respond_failure("insufficient_money", "You don't have enough money to buy this ship."));
		}
	}
} // Since we have required sessionActive we can assume that $userId is a valid user.