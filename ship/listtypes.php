<?php

require_once("../includes/sessionActive.php");
require_once("../includes/apiresponse.php");
require_once("../includes/dbconnect.php");

$sql = "SELECT id, name, class, cost
FROM ship_types";
$result = $conn->query($sql);

$returnData = array();

if ($result !== false) {
	while ($row = $result->fetch_assoc()) {
		$returnData[] = array(
			"id" => intval($row["id"]),
			"name" => $row["name"],
			"class" => intval($row["class"]),
			"cost" => intval($row["cost"])
		);
	}
}

echo(respond_success($returnData));
