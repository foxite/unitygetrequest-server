<?php

require_once("../includes/sessionActive.php");
require_once("../includes/apiresponse.php");
require_once("../includes/dbconnect.php");

$sql = "SELECT s.id, s.hull_upgrades, s.armor_upgrades, s.speed_upgrades, s.kinetic_upgrades, s.laser_upgrades, s.missile_upgrades, s.missile_count_upgrades, t.name AS type_name, t.class
FROM ships AS s
LEFT JOIN ship_types AS t ON (s.ship_type = t.id)
WHERE owner = {$userId}";
$result = $conn->query($sql);

$returnData = array();

if ($result !== false) {
	//$returnData = $result->fetch_all(MYSQLI_ASSOC); // This makes int fields strings
	while ($row = $result->fetch_assoc()) {
		$returnData[] = array(
			"id" => intval($row["id"]),
			"hull_upgrades" => intval($row["hull_upgrades"]),
			"armor_upgrades" => intval($row["armor_upgrades"]),
			"speed_upgrades" => intval($row["speed_upgrades"]),
			"kinetic_upgrades" => intval($row["kinetic_upgrades"]),
			"laser_upgrades" => intval($row["laser_upgrades"]),
			"missile_upgrades" => intval($row["missile_upgrades"]),
			"missile_count_upgrades" => intval($row["missile_count_upgrades"]),
			"type_name" => $row["type_name"],
			"class" => intval($row["class"])
		);
	}
}

echo(respond_success($returnData));
