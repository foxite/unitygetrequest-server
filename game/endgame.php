<?php

require_once("../includes/sessionActive.php");
require_once("../includes/apiresponse.php");
require_once("../includes/dbconnect.php");

if (empty($_POST["score"]) || !is_numeric($_POST["score"]) || intval($_POST["score"]) < 0) {
	die(respond_failure("client_error", "Required parameter missing"));
}

$score = intval($_POST["score"]);

$sql = "SELECT money FROM accounts WHERE id = {$userId}";
$result = $conn->query($sql);
$row = $result->fetch_assoc();
$oldMoney = intval($row["money"]);
$newMoney = $oldMoney + $score;

$sql = "UPDATE accounts SET money = {$newMoney} WHERE id = {$userId}";
$conn->query($sql);
// We don't actually prevent the client from saying "yeah i just got 6538172349539120985 points" but that would have to be a server that's not running in PHP
echo(respond_success($newMoney));
